<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

[//]: # (<p align="center">)

[//]: # (<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>)

[//]: # (<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>)

[//]: # (<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>)

[//]: # (<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>)

[//]: # (</p>)

## O проекте

Проект был весьма интересным и обучающим. Спасибо вам за это.

- Использовал Laravel 8
- [Проект выложил на своем репозитории gitlab](https://gitlab.com/Haruteghiazaryan1987/data-sfera).
- БД развернул на бесплатном хостинге sprinthost.ru. Доступ панели [PhpMyAdmin](https://pma.sprinthost.ru/) <br>&#8195;&#8195;<i>Пользователь:</i>&#8194;<b>f0834050_datasfera</b><br>&#8195;&#8195;<i>Пароль:</i>&#8194;<b>datasfera</b>


## Информация

Хотелось бы обратить ваше внимание на мой проект <b><i>Сайт Резюме</i></b>. В целях обучения Vue часть [фронтенда](https://gitlab.com/Haruteghiazaryan1987/cv-web) разработал на Vue3 а [бекенд](https://gitlab.com/Haruteghiazaryan1987/cv-backend) на Laravel 9 с интеграцией API.

## Просьба

Если в будущем будет возможность присылать мне оптимальное решение этой задачи (линк на репу, zip и тп) буду очень благодарен. Для меня это очень важно. 
<br>
<br>
<h1 style="color:red;text-align:right;">Спасибо</h1>

