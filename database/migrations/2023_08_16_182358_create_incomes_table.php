<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incomes', function (Blueprint $table) {
            $table->id();
            $table->integer('income_id')->unsigned();
            $table->string('number',100);
            $table->date('date');
            $table->date('last_change_date');
            $table->string('supplier_article',100);
            $table->string('tech_size',100);
            $table->string('barcode',100);
            $table->smallInteger('quantity')->unsigned();
            $table->mediumInteger('total_price');
            $table->date('date_close');
            $table->string('warehouse_name',100);
            $table->integer('nm_id')->unsigned();
            $table->string('status',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incomes');
    }
}
