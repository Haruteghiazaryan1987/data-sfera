<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('g_number',100);
            $table->dateTime('date');
            $table->dateTime('last_change_date');
            $table->string('supplier_article',100);
            $table->string('tech_size',100);
            $table->string('barcode',100);
            $table->mediumInteger('total_price');
            $table->smallInteger('discount_percent')->unsigned();
            $table->string('warehouse_name',100);
            $table->string('oblast',100);
            $table->integer('income_id')->unsigned();
            $table->bigInteger('odid')->unsigned();
            $table->integer('nm_id')->unsigned();
            $table->string('subject',100);
            $table->string('category',100);
            $table->string('brand',100);
            $table->boolean('is_cancel');
            $table->date('cancel_dt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
