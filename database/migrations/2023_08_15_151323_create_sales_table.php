<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->string('g_number',100);
            $table->date('date');
            $table->date('last_change_date');
            $table->string('supplier_article',100);
            $table->string('tech_size',100);
            $table->string('barcode',100);
            $table->mediumInteger('total_price');
            $table->smallInteger('discount_percent')->unsigned();
            $table->boolean('is_supply');
            $table->boolean('is_realization');
            $table->mediumInteger('promo_code_discount')->unsigned();
            $table->string('warehouse_name',100);
            $table->string('country_name',100);
            $table->string('oblast_okrug_name',100);
            $table->string('region_name',100);
            $table->integer('income_id')->unsigned();
            $table->string('sale_id',100);
            $table->bigInteger('odid')->unsigned();
            $table->mediumInteger('spp')->unsigned();
            $table->float('for_pay');
            $table->float('finished_price',10,2);
            $table->float('price_with_disc');
            $table->integer('nm_id')->unsigned();
            $table->string('subject',100);
            $table->string('category',100);
            $table->string('brand',100);
            $table->boolean('is_storno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
