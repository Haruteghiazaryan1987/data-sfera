<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->date('last_change_date');
            $table->string('supplier_article',100);
            $table->string('tech_size',100);
            $table->string('barcode',100);
            $table->smallInteger('quantity')->unsigned();
            $table->boolean('is_supply');
            $table->boolean('is_realization');
            $table->smallInteger('quantity_full')->unsigned();
            $table->string('warehouse_name',100);
            $table->smallInteger('in_way_to_client')->unsigned();
            $table->smallInteger('in_way_from_client')->unsigned();
            $table->integer('nm_id')->unsigned();
            $table->string('subject',100);
            $table->string('category',100);
            $table->string('brand',100);
            $table->string('sc_code',100);
            $table->float('price',10,2);
            $table->smallInteger('discount')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
