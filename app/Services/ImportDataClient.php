<?php

namespace App\Services;

use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class ImportDataClient
{
    public $client;
    private $baseUri;
    private $key;

    public function __construct()
    {
        $this->baseUri = env("DATA_SFERA_URI");
        $this->key = env("DATA_SFERA_KEY");

        $this->client = Http::baseUrl($this->baseUri);
    }

    /**
     * @throws RequestException
     */
    public function getData($dataUrl, $dateFrom, $dateTo, $page, $limit)
    {

        $response = $this->client->get($dataUrl, [
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'page' => $page,
            'key' => $this->key,
            'limit' => $limit,
        ]);

        return $response->throw()->json();
    }


}
