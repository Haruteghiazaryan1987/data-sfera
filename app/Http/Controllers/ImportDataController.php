<?php

namespace App\Http\Controllers;

use App\Jobs\ImportDataIncomesJob;
use App\Jobs\ImportDataOrdersJob;
use App\Jobs\ImportDataSalesJob;
use App\Jobs\ImportDataStocksJob;

class ImportDataController extends Controller
{
    public function index()
    {
        ImportDataSalesJob::dispatch();
        ImportDataOrdersJob::dispatch();
        ImportDataStocksJob::dispatch();
        ImportDataIncomesJob::dispatch();

        return view('welcome');
    }
}
