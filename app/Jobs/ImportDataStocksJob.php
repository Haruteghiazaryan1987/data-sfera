<?php

namespace App\Jobs;

use App\Services\ImportDataClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Client\RequestException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportDataStocksJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3600;
    public $tries = 2;
    public $backoff = 30;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws RequestException
     */
    public function handle(ImportDataClient $importData)
    {
        $i = 1;
        do {
            $data = $importData->getData('stocks', '2023-08-16', '2023-08-16', $i, 500);

            $result = DB::table('stocks')
                ->insert(
                    $data['data']
                );

            if (!$result) {
                $this->fail();
                DB::table('stocks')->truncate();
                exit();
            }

            sleep(50);
            $i++;
        } while ($data['links']['next'] != null);
    }
}
