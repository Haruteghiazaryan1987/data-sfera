<?php

namespace App\Jobs;

use App\Services\ImportDataClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Client\RequestException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportDataSalesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3600;
    public $tries = 2;
    public $backoff = 30;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws RequestException
     */
    public function handle(ImportDataClient $importData)
    {
        $i = 1;
        do {
            $data = $importData->getData('sales', '2020-07-14', '2023-08-15', $i, 500);

            $result = DB::table('sales')
                ->insert(
                    $data['data']
                );

            if (!$result) {
                $this->fail();
                DB::table('sales')->truncate();
                exit();
            }

            if ($i % 30 === 0 && $data['links']['next'] != null) {
                sleep(15);
            }

            $i++;
        } while ($data['links']['next'] != null);
    }
}
